import consola from 'consola'

export const state = () => ({
  products: [],
  product: {},
  minMaxPricing: []
})

export const mutations = {
  set(state, { key, payload }) {
    state[key] = payload
  }
}

export const actions = {
  async getAllProducts({ commit }, options) {
    if (!options) {
      options = {
        isAll: false,
        categoryId: null,
        gte: null,
        lte: null
      }
    }

    const { isAll, categoryId, gte, lte } = options

    try {
      const data = await this.$axios.$get(
        `/api/v1/products?isAll=${isAll}&categoryId=${categoryId}&gte=${gte}&lte=${lte}`
      )

      commit('set', { key: 'products', payload: data })
    } catch (err) {
      consola.error('Geting all products error: ', err)
    }
  },
  async getProductById({ commit }, id) {
    try {
      const data = await this.$axios.$get(`/api/v1/products/${id}`)

      commit('set', { key: 'product', payload: data })
    } catch (err) {
      consola.error('Getting product by id error: ', err)
    }
  },
  async getProductBySlug({ commit }, slug) {
    try {
      const data = await this.$axios.$get(`/api/v1/products/slug/${slug}`)

      commit('set', { key: 'product', payload: data })
    } catch (err) {
      consola.error('Getting product by slug error: ', err)
    }
  },
  async getMinMaxPricing({ commit }, categorySlug) {
    try {
      const data = await this.$axios.$get(
        `/api/v1/products/minmax/pricing?categorySlug=${categorySlug}`
      )

      commit('set', { key: 'minMaxPricing', payload: data })
    } catch (err) {
      consola.error('Getting min and max product pricing error: ', err)
    }
  },
  async updateProduct({ state }, isUpdate) {
    const { product } = state

    try {
      if (isUpdate) {
        await this.$axios.put(`/api/v1/products`, product)
      } else {
        await this.$axios.post(`/api/v1/products`, product)
      }
    } catch (err) {
      consola.error('Updating product error: ', err)
    }
  },
  async removeProduct({ dispatch }, id) {
    try {
      await this.$axios.delete(`/api/v1/products/${id}`)

      await dispatch('getAllProducts')
    } catch (err) {
      consola.error('Removing product error: ', err)
    }
  }
}
