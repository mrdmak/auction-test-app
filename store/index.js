export const state = () => ({
  title: 'Demo App',
  subtitle: ''
})

export const mutations = {
  set(state, { key, payload }) {
    state[key] = payload
  }
}
