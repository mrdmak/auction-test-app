import consola from 'consola'

export const state = () => ({
  categories: [],
  category: {}
})

export const mutations = {
  set(state, { key, payload }) {
    state[key] = payload
  }
}

export const actions = {
  async getAllCategories({ commit }, options) {
    if (!options) {
      options = { isAll: false }
    }

    const { isAll } = options

    try {
      const data = await this.$axios.$get(`/api/v1/categories?isAll=${isAll}`)

      commit('set', { key: 'categories', payload: data })
    } catch (err) {
      consola.error('Getting all categories error: ', err)
    }
  },
  async getCategoryById({ commit }, id) {
    try {
      const data = await this.$axios.$get(`/api/v1/categories/${id}`)

      commit('set', { key: 'category', payload: data })
    } catch (err) {
      consola.error('Getting category by id error: ', err)
    }
  },
  async getCategoryBySlug({ commit }, slug) {
    try {
      const data = await this.$axios.$get(`/api/v1/categories/slug/${slug}`)

      commit('set', { key: 'category', payload: data })
    } catch (err) {
      consola.error('Getting category by slug error: ', err)
    }
  },
  async updateCategory({ state }, isUpdate) {
    const { category } = state

    try {
      if (isUpdate) {
        await this.$axios.put(`/api/v1/categories`, category)
      } else {
        await this.$axios.post(`/api/v1/categories`, category)
      }
    } catch (err) {
      consola.error('Updating category error: ', err)
    }
  },
  async removeCategory({ dispatch }, id) {
    try {
      await this.$axios.delete(`/api/v1/categories/${id}`)

      await dispatch('getAllCategories')
    } catch (err) {
      consola.error('Deleting category error: ', err)
    }
  }
}
