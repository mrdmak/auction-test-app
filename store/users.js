import consola from 'consola'

export const state = () => ({
  users: [],
  user: {}
})

export const mutations = {
  set(state, { key, payload }) {
    state[key] = payload
  }
}

export const actions = {
  async getAllUsers({ commit }) {
    try {
      const data = await this.$axios.$get(`/api/v1/users`)

      commit('set', { key: 'users', payload: data })
    } catch (err) {
      consola.error('Getting all users error: ', err)
    }
  },
  async getUserById({ commit }, id) {
    try {
      const data = await this.$axios.$get(`/api/v1/users/${id}`)

      commit('set', { key: 'user', payload: data })
    } catch (err) {
      consola.error('Geting user by id error: ', err)
    }
  },
  async updateUser({ state }, isUpdate) {
    const { user } = state

    try {
      if (isUpdate) {
        await this.$axios.put(`/api/v1/users/${user.id}`, user)
      } else {
        await this.$axios.post(`/api/v1/users`, user)
      }
    } catch (err) {
      consola.error('Updating user error: ', err)
    }
  }
}
