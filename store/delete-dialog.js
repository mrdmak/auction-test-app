export const state = () => ({
  showDialog: false,
  selectedForDelete: null
})

export const mutations = {
  set(state, { key, payload }) {
    state[key] = payload
  }
}

export const actions = {
  toggleShowDialog({ state, commit }, entity) {
    commit('set', { key: 'selectedForDelete', payload: entity })
    commit('set', { key: 'showDialog', payload: !state.showDialog })
  }
}
