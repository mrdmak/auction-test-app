export default {
  main: {
    categories: 'Категории',
    products: 'Товары'
  },
  category: {
    filterByPrice: 'Фильтровать по цене',
    apply: 'Применить'
  }
}
