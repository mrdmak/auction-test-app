export default {
  main: {
    categories: 'Категорії',
    products: 'Товари'
  },
  category: {
    filterByPrice: 'Фильтрувати по ціні',
    apply: 'Застосувати'
  }
}
