export default {
  methods: {
    getByLocale(translations, field) {
      if (translations === undefined) {
        return ''
      }

      const result = translations.filter(
        (translation) => translation.locale === this.$i18n.locale
      )

      if (result.length !== 0) {
        return result[0][field]
      }

      return ''
    }
  }
}
